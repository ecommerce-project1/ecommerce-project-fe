export default {
  oidc: {
    clientId: '0oatwv7t7N4PftTNw5d6',
    issuer: 'https://dev-73062661.okta.com/oauth2/default',
    redirectUri: 'http://localhost:4200/login/callback',
    scopes: ['openid', 'profile', 'email']
  }
};
